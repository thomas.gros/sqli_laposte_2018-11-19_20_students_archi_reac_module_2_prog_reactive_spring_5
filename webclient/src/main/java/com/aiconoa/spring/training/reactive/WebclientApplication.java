package com.aiconoa.spring.training.reactive;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class WebclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebclientApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner reactiveclient() {
		return args -> {
		
			WebClient
				.create("http://localhost:8080")
				.get()
				.uri("/list")
				.retrieve()
				// si type générique, passer par ParameterizedTypeReference
				.bodyToFlux(Integer.class)
				.subscribe(System.out::println);
			
			System.in.read();

			//  Thread.currentThread().join(); // autre astuce
			
		};
	}
}
