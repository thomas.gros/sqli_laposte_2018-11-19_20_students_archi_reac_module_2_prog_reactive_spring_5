package com.aiconoa.spring.training.reactor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Flux;

//@Component
public class FirstExample implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		
		Flux
			.just(1,2,3,4,5)
			//.subscribe(e -> System.out.println(e));
			.subscribe(System.out::println);
		
	}
	
}
