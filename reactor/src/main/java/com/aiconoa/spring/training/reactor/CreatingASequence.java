package com.aiconoa.spring.training.reactor;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//@Component
public class CreatingASequence implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		/******
		 * FLUX
		 *****/
		// Flux.just : envoie les éléments puis signal complete
		System.out.println("#### Flux.just ####");
		Flux
			.just(1, 2, 3)
			.subscribe(
					System.out::println,
					e -> { /* NOOP */ },
					() -> { System.out.println("complete"); }		
			);
		
		// Flux.fromArray
		System.out.println("#### Flux.fromArray ####");
		Integer[] ints = { 1, 2, 3 };
		Flux
			.fromArray(ints)
			.subscribe(
					System.out::println,
					e -> { /* NOOP */ },
					() -> { System.out.println("complete"); }		
			);
		
		// Flux.fromStream
		System.out.println("#### Flux.fromStream ####");
		Flux.fromStream(Arrays.asList(1,2,3).stream())
			.subscribe(
					System.out::println,
					e -> { /* NOOP */ },
					() -> { System.out.println("complete"); }		
			);
		
		/******
		 * Mono
		 *****/
		System.out.println("#### Mono.just ####");
		Mono
			.just("hello world")
			.subscribe(System.out::println);
		
		System.out.println("#### Mono.justOrEmpty(null) ####");
		Mono
			.justOrEmpty(null)
			.subscribe(
					System.out::println,
					e -> { /* NOOP */ },
					() -> { System.out.println("complete"); }		
			);
		
//		Mono
//			.just(null) // null non autorisé
//			.subscribe(System.out::println);
		
		
	}
	
}
