package com.aiconoa.spring.training.reactor;

import java.time.Duration;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Flux;

//@Component
public class ErrorHandling implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {

		Flux
		.just(1,2,3,4)
		.map(e -> {
			if(e < 4) return e * 2;
			throw new RuntimeException("boom");
			
			// dans le cadre des CheckedException => gérer avec try/catch
			// par exemple: reproprager l'exception
			// dans reactor-extra  classes utilitaires Exceptions.propagate(e)
//			try {
//				new SqlException()
//			} catch(Exception e) {
//				throw new RuntimeException(e);
//			}
		})
		.subscribe(
				System.out::println,
				e -> e.printStackTrace()
		);
		
		
		Flux
		.just(1,2,3,4)
		.map(e -> {
			if(e < 4) return e * 2;
			throw new RuntimeException("boom");
		})
		.onErrorReturn(-1) // fallback value
		.subscribe(
				System.out::println,
				e -> e.printStackTrace()
		);
		
		Flux
		.just(1,2,3,4)
		.map(e -> {
			if(e < 4) return e * 2;
			throw new RuntimeException("boom");
		})
		.onErrorResume(e -> Flux.just(10, 20, 30))
		.subscribe(
				System.out::println,
				e -> e.printStackTrace()
		);
		
		Flux
		.just(1,2,3,4)
		.map(e -> {
			if(e < 4) return e * 2;
			throw new RuntimeException("boom");
		})
		.retry(2)
		.subscribe(
				System.out::println,
				e -> e.printStackTrace()
		);
		
		
		Flux
		.just(1,2,3,4)
		.delayElements(Duration.ofSeconds(1))
		.timeout(Duration.ofMillis(500))
		.subscribe(
				System.out::println,
				e -> e.printStackTrace()
		);
		
		Thread.sleep(3000);
		
		
		
//		userMovieService
//		.getFavoritesForUser(userId) // Flux<id>
//		.timeout(Duration.ofMillis(500))
//		.onErrorResume(userLocalCachedFavorites.getCachedFavorite())
//		.switchIfEmpty(movieSuggestionService.getSuggestion()) // Flux<id>
//		.flatMap(id -> movieDetailService.detail(id))
//		.take(5)
//		.subscribe(ui::ShowMovies)
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
}
