package com.aiconoa.spring.training.reactor;

import java.time.Duration;
import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

//@Component
public class TransformingASequence implements CommandLineRunner {

	public String alphabet(int letterNumber) {
		if (letterNumber < 1 || letterNumber > 26) {
			return null;
		}
		int letterIndexAscii = 'A' + letterNumber - 1;
		return "" + (char) letterIndexAscii;
	}
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Transformer une séquence");

		System.out.println("### map");
		Flux
			.just(1, 2, 3) // upstream
			.map(d -> d * 2)
			.subscribe( // downstream
					System.out::println,
					e -> { /* NOOP */ },
					() -> { System.out.println("complete"); }		
			);
		
		
		Flux
			.just(new Person("Thomas", "Gros", 38), new Person("John", "Doe", 24))
			.map(p -> p.getFirstName() + " " + p.getLastName())
			.subscribe(
					System.out::println,
					e -> { /* NOOP */ },
					() -> { System.out.println("complete"); }		
			);
		
		
		
		Flux.range(0, 10)
			.startWith(10, 20, 30)
			.startWith(100, 200, 300)
			.concatWith(Flux.range(1000, 10))
			.subscribe(System.out::println);
		
		
		Flux
			.just("a", "b", "c")
			.timestamp()
			.map(t -> t.getT1())
			.index()
			.subscribe(System.out::println);
		
		Flux.just("a", "b", "c")
			.map(String::toUpperCase)
			.collectList() // Mono<List<String>>
			.subscribe(System.out::println);
		
		Flux
			.just("a", "b", "c")
			.count()
			.subscribe(System.out::println); // Mono<Long>
		
		Flux
			.range(0,  10)
			.reduce(0, (acc, ele) -> {
				return acc + ele;
			})
			.subscribe(System.out::println);
		

		Flux
			.range(0,  10)
			.scan(0, (acc, ele) -> {
				return acc + ele;
			})
			.subscribe(System.out::println);
		
		
		Flux
			.just(1,2,3)
			.mergeWith(Flux.just(4,5,6))
			.subscribe(System.out::println);
		
		// Flux.merge(Flux.just(1,2,3), Flux.just(4,5,6));
		
		Flux
			.mergeSequential(Flux.just(1,2,3), Flux.just(4,5,6))
			.subscribe(System.out::println);
		
		Flux
			.just("a", "b", "c")
			.zipWith(Flux.just(1,2,3,4))
			.subscribe(System.out::println);
		
		Flux
			.just(1,2,3)
			.flatMap(e -> Flux.range(0, e))
			.subscribe(System.out::println);
		
		Flux
			.just(1,2,3)
			.flatMap(e -> Flux
							.range(0, e)
							.map(d -> Arrays.asList(e, d))) // reactor-extra pour produire des tuples
			.subscribe(System.out::println);
		
		
		Flux
			.just(1,2,3)
			.repeat(4)
			.subscribe(System.out::println);
		
		Flux
			.just(1,2,3)
			.all(e -> e > 2) // any
			.subscribe(System.out::println);
		
		
		Flux
			.range(0, 15)
			.buffer(6)
			.subscribe(System.out::println); // insert dans DataBase
		
//		Flux
//			.range(0, 15)
//			.delayElements(Duration.ofMillis(200)) // Signals are delayed and continue on the parallel default Scheduler,
//			.buffer(Duration.ofMillis(500))
//			.subscribe(System.out::println);
//		
//		Thread.sleep(4000);
		
//		Flux
//		.range(0, 15)
//		.delayElements(Duration.ofMillis(200)) // Signals are delayed and continue on the parallel default Scheduler,
//		.window(Duration.ofMillis(500))
//		.subscribe(System.out::println);
//	
//		Thread.sleep(4000);
		
		
//		Flux
//			.range(0, 15)
//			.delayElements(Duration.ofMillis(200)) // Signals are delayed and continue on the parallel default Scheduler,
//			.window(Duration.ofMillis(500), Duration.ofMillis(300))
//			.collectList()
//			.subscribe(System.out::println);
//	
//		Thread.sleep(4000);
		
		
		
		
//		Flux
//			.just("url-1","url-2","url-3")
//			.flatMap(url -> Flux
//							.range(0, 3)
//							.map(e -> url + e))
//			.subscribe(System.out::println);
		
		
		
		// step 1 de flatmap
		// Flux.range(0, 1) -> Flux.range(0, 2) -> Flux.range(0, 3)
		
		// step 2 de flatmap
		// 0 -> 0 -> 1 -> 0 -> 1 -> 2
//		
//		
//		
//		Flux.just("a","b","c").cast(Object.class).subscribe(System.out::println);
//		Flux.just("a","b","c").index().subscribe(System.out::println);
//		
//		Flux.just(1,2,3)
//			.flatMap(e -> Flux.range(10,  e))
//			.subscribe(System.out::println);
//	
//		// scatter / gather
//		Flux.just("http://www.example.com", "http://httpbin.org")
//			.flatMap(url -> WebClient.create(url).get().uri("/").retrieve().bodyToFlux(String.class))
//			.subscribe(System.out::println);
//		// TODO wait, par ex: // 
//		System.in.read();
//		
//		
//		
//		// Handle https://projectreactor.io/docs/core/release/reference/#_handle
//		Flux<String> alphabet = Flux.just(-1, 30, 13, 9, 20)
//			    .handle((i, sink) -> {
//			        String letter = alphabet(i); 
//			        if (letter != null) 
//			            sink.next(letter); 
//			    });
//
//			alphabet.subscribe(System.out::println);
//		
//		Flux.just(1,2,3).concatWith(Flux.just(7,8,9)).startWith(4,5,6).subscribe(System.out::println);
//		Flux.just(1,2,3).collectList().subscribe(System.out::println);
//		Flux.just(1,2,3).all(e -> e < 3).subscribe(System.out::println);
//		Flux.just(1,2,3).buffer(2).subscribe(System.out::println);
//		
//		
//		Flux.just(1,2,3,4).zipWith(Flux.just("a","b","c")).subscribe(
//				System.out::println,
//				e -> {},
//				() -> System.out.println("complete"));
//		
//		Flux.just(1,2,3,4).delayElements(Duration.ofMillis(100)).repeat(3).subscribe(System.out::println);
//		System.in.read();
		
		
		Flux
			.just(new Person("Thomas", "Gros", 38), 
				  new Person("John", "Doe", 24),
				  new Person("Bob", "Smith", 46),
				  new Person("Helen", "Wild", 54)
			)
			.groupBy(p -> p.getAge() > 30 ? "vieux" : "jeune") // Flux<GroupeFlux>
			.concatMap(g -> g.map(String::valueOf)   // ou Person::toString
							 .startWith(g.key()))
			.subscribe(System.out::println);
		
		
		Flux
			.range(0, 15)
			.filter(e -> e % 2 == 0)
			.subscribe(System.out::println);
		
		
		Flux
			.just(1,2,3,1,2,3)
			.distinct()
			.subscribe(System.out::println);
		
		
		Flux
			.range(0, 15)
			.take(10) // idem avec duration
			.subscribe(System.out::println);
		
		
		Flux
		.range(0, 15)
		.takeUntil(e -> e > 8)
		.subscribe(System.out::println);
		
		
		Flux
		.range(0, 15)
		.takeLast(3)
		.subscribe(System.out::println);
		
		Flux
		.range(0, 15)
		.skip(5)
		.subscribe(System.out::println);		
		
		
//		Flux
//		.range(0, 15)
//		.delayElements(Duration.ofMillis(200))
//		.sample(Duration.ofMillis(300))
//		.subscribe(System.out::println);
//		
//		Thread.sleep(5000);
		
		Flux
			//.just(1,2,3)
			.empty()
			.switchIfEmpty(Flux.just(4,5,6))
			.subscribe(System.out::println);
		
		
		
		
		
		// TODO example
		/*
		 * 
		 * userService.getFavorites(userId)
		 * .timeout(Duration.ofMillis(800))
		 * .onErrorResume(cacheService.cachedFavoritesFor(userId))
		 * .flatMap(favoriteService::getDetails)
		 * .switchIfEmpty(suggestionService.getSuggestions()
		 * .take(5)
		 * .publishOn(UiUtils.uiThreadScheduler()
		 * .subscribe(uiList::show, UiUtils::errorPopup);
		 * 
		 * 
		 * 
		 */
		
		
		// TODO example flatMap type scatter/gather sur request HTTP
		// TODO exponential backoff (cf docs)
	}
	
}
