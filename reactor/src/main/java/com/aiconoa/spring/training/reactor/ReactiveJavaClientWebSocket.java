package com.aiconoa.spring.training.reactor;

import java.net.URI;
import java.time.Duration;

import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;

public class ReactiveJavaClientWebSocket {
	  
    public static void main(String[] args) throws InterruptedException {
 
        
        WebSocketClient client = new ReactorNettyWebSocketClient();
        client.execute(
          URI.create("ws://localhost:8080/path"), 
          session -> session
          				.receive()
          				.map(WebSocketMessage::getPayloadAsText)
          				//.log()
          				.then())
        .block();
          
          
    }
}