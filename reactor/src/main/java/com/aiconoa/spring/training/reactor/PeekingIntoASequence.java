package com.aiconoa.spring.training.reactor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Flux;

@Component
public class PeekingIntoASequence implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {

		Flux
		.just(1,2,3,4)
		.doOnNext(e -> System.out.println("just -> " + e))
		.map(e -> e * 2)
		.doOnNext(e -> System.out.println("map -> " + e))
		.subscribe(
				System.out::println,
				e -> e.printStackTrace()
		);
	
		
		
		Flux
		.just(1,2,3,4)
		.map(e -> e * 2)
		.log()
		.subscribe(
				System.out::println,
				e -> e.printStackTrace()
		);
		
		
	}
	
}
