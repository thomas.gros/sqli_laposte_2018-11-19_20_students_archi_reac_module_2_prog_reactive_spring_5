package com.aiconoa.spring.training.reactive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;

import reactor.core.publisher.Flux;

@SpringBootApplication
@EnableWebFlux
@EnableReactiveMongoRepositories
public class WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}
	
	@Autowired
	private MyMongoRepo mongoRepo;

	@Bean
	public CommandLineRunner preLoadMongo() throws Exception {
		return args -> {
			mongoRepo.saveAll(
							Flux.just(new Account(null, "Bill", 12.3), 
									  new Account(null, "Thomas", 14.3),
									  new Account(null, "Bobb", 10.3)))
					.thenMany(mongoRepo.findAll().map(a -> a.toString()))
					.subscribe(System.out::println);
		};
	}
}
