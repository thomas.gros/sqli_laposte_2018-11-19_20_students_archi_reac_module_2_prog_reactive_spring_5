package com.aiconoa.spring.training.reactive;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

//CrudReactiveRepository
public interface MyMongoRepo extends ReactiveMongoRepository<Account, String> {
}
