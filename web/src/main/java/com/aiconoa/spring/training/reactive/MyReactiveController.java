package com.aiconoa.spring.training.reactive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import reactor.core.publisher.Flux;

// @RestController = @Controller + @ReponseBody
@Controller
public class MyReactiveController {
	
	@Autowired
	private MyMongoRepo mongoRepo;
	
	@GetMapping("/accounts")
	@ResponseBody
	public Flux<Account> getAllAccounts() {
		return mongoRepo.findAll();
	}
	

	@GetMapping("/list")
	@ResponseBody
	public Flux<Integer> getList() {
		return Flux.range(0,100000);
	}
	
	@GetMapping(path="/list-stream-json", 
			    produces=MediaType.APPLICATION_STREAM_JSON_VALUE)
	@ResponseBody
	public Flux<String> getListStreamJson() {
		return Flux.range(0,100).map(d -> "{id:"+d+"}");
	}
	
	@GetMapping(path="/list-sse", 
		    	produces=MediaType.TEXT_EVENT_STREAM_VALUE)
	@ResponseBody
	public Flux<String> getListServerSentEvent() {
		return Flux.range(0,100).map(d -> "{id:"+d+"}");
	}
	
	@GetMapping(path="/list-sse-standard", 
	    		produces=MediaType.TEXT_EVENT_STREAM_VALUE)
	@ResponseBody
	public Flux<ServerSentEvent<Integer>> getListServerSentEventStandard() {
		return Flux.range(0,100)
				   .map(d -> ServerSentEvent
						   		.<Integer>builder()
						   		.event("demo")
						   		.data(d)
						   		.id("" + d)
						   		.build());
	}
	
}