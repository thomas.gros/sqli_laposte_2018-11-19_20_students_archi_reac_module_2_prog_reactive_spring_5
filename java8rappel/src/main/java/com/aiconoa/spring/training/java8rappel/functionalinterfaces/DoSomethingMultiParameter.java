package com.aiconoa.spring.training.java8rappel.functionalinterfaces;

@FunctionalInterface
public interface DoSomethingMultiParameter {
	String doAThing(String p1, String p2);
	
	default void somethingElse() {
		System.out.println("et oui c'est possible");
	}
	
}
