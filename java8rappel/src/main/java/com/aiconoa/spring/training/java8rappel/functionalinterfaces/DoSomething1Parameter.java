package com.aiconoa.spring.training.java8rappel.functionalinterfaces;

@FunctionalInterface
public interface DoSomething1Parameter {

	String doAThing(String param);
}
