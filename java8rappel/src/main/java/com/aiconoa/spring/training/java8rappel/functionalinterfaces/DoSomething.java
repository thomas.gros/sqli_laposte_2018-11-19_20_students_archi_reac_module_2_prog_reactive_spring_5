package com.aiconoa.spring.training.java8rappel.functionalinterfaces;

@FunctionalInterface
public interface DoSomething {
	String doAThing();
}
