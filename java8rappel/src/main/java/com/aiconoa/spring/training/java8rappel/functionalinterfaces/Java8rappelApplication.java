package com.aiconoa.spring.training.java8rappel.functionalinterfaces;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Java8rappelApplication {

	public static void main(String[] args) {
		SpringApplication.run(Java8rappelApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner functionalInterfacesTheory() {
//		return new CommandLineRunner() {
//			
//			@Override
//			public void run(String... args) throws Exception {
//				System.out.println("hello");
//			}
//		};
		
		DoSomething dsPreJava8 = new DoSomething() {
			
			@Override
			public String doAThing() {
				return "hello";
			}
		};
		
		// Lambda Expression correspond à un type 
		// définit par une interface fonctionnelle
		DoSomething dsJava8 = () -> {
			return "hello";
		};
		
		// Si 1 ligne dans la lambda, enlever les accolades et le return
		DoSomething dsJava8version2 = () -> "hello";
		
		
		DoSomething1Parameter sth1param = (String param) -> "hello" + param;
		DoSomething1Parameter sth1paramversion2 = param -> "hello" + param;
		
		DoSomethingMultiParameter sthMulti = (p1, p2) -> "hello" + p1 + " " + p2;
		
		DoSomethingMultiParameter sthMultiComplex = (p1, p2) -> {
			String res = "hello" + p1 + " " + p2;
			return res;
		};
		
		sthMultiComplex.doAThing("a", "b");
		sthMultiComplex.somethingElse(); // appel de la méthode définie par défaut sur l'interface
		
		
		return args -> System.out.println("hello");	
	}
	
	@Bean
	public CommandLineRunner functionalInterfacesPractical() {
		return args -> {
			
			List<Integer> data = Arrays.asList(5,3,4,1,2);

			Comparator<Integer> myComparator = (i1, i2) -> {
				if(i1 > i2) return 1;
				else if(i1 < i2) return -1;
				else return 0;	
			};
			
			data.sort(myComparator);
			
			System.out.println(data);
		};
	}
	
	@Bean
	public CommandLineRunner commonFunctionalInterfaces() {
		return args -> {
			// interfaces existante avant Java 8: Runnable, Comparator,...
			Runnable run = () -> { System.out.println("inside the runnable"); };
			// new Thread(run);
			
			// ajouts Java 8 pour la programmation fonctionnelle
			Predicate<String> isNotEmpty = s -> s.length() > 0; 
			isNotEmpty.test("hello");
			isNotEmpty.negate().test("hello");
			
			
			Function<String, Integer> length = s -> s.length();
			
			Supplier<Integer> supplier = () -> 42;
			
			Consumer<Integer> consumer = (i) -> System.out.println(i);
		};
	}

	@Bean
	public CommandLineRunner streamExamples() {
		
		return (args) -> {
		
			List<String> myList = 
					Arrays.asList("cat", "dog", "horse", "chien", "chat");
			

			// Stream<String> s = myList.stream();
			
			// Stream<String> s2 = s.filter(d -> d.startsWith("c"));

//			Predicate<String> pred = d -> d.startsWith("c");
//			
//			Predicate<String> predComplet = new Predicate<String>() {
//				public boolean test(String d) {
//					return d.startsWith("c");
//				}
//			};
			
			// s.filter(d -> d.length() < 3); // attention ici on enchaine pas les filtree
			
			// s2.filter(d -> d.length() < 3);	// ici on enchaine les filtres
			
			// style fluent, continu
			Stream<String> pipeline = myList
								//.stream()
								.parallelStream()
								.filter(d -> d.startsWith("c"))
								.filter(d -> d.length() > 2)
								.map(d -> d.toUpperCase());
			pipeline.forEach(d -> System.out.println(d));
			// pipeline.collect(collector)
			
			// pipeline.forEach(d -> System.out.println("coucou"));
			
			// Autre examples
//			Files.lines(Paths.get("uri de mon fichier"))
//				 .map(s -> s.toUpperCase())
//				 .forEach(s -> System.out.println(s));
			
//			try (InputStream is = new URL("http://www.example.com").openStream();
//				     BufferedReader reader = new BufferedReader(new InputStreamReader(is));
//				     Stream<String> stream = reader.lines()) {
//				    stream.map(d -> d.toUpperCase()).forEach(System.out::println);
//				}
	
		};
		
		
	}
	
	@Bean
	public CommandLineRunner methodReference() {
		return args -> {
			List<String> myList = 
					Arrays.asList("cat", "dog", "horse", "chien", "chat");
			
			myList
				.stream()
//				.forEach(d -> System.out.println(d));
				.forEach(System.out::println);
			
			Consumer<String> s = System.out::println;
			
			myList
				.stream()
				//.map(d -> d.toUpperCase());
				.map(String::toUpperCase)
				.collect(Collectors.toList());
			
			
			System.out.println("hello world");
			
		};
	}
	
}
